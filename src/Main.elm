module Main exposing (..)

import Browser
import Html


main : Program () () msg
main =
    Browser.element
        { init = always ( (), Cmd.none )
        , update = always (always ( (), Cmd.none ))
        , subscriptions = always Sub.none
        , view = always (Html.text <| toString <| toGame <| emptyTTT)
        }


type BWE
    = B
    | W
    | E


type alias TTT =
    ( ( BWE, BWE, BWE ), ( BWE, BWE, BWE ), ( BWE, BWE, BWE ) )


toList : TTT -> List BWE
toList ( ( a, b, c ), ( d, e, f ), ( g, h, i ) ) =
    [ a, b, c, d, e, f, g, h, i ]


toGame : TTT -> Game
toGame p =
    toGameRec identity p

toGameRec: (Game -> Game) -> TTT -> Game
toGameRec k p_ =
    case (moves B p_, moves W p_) of
        ([], []) ->
            k zero

        (bp::bps, _) ->
            toGameRec (\bg -> bps |> List.map (\bp_ -> \k_ -> k_ (merge (toGameRec )))) bp


        ([], wp::wps) ->
            toGameRec (\wg -> wps |> List.map (toGameRec rotateRight) |> List.foldl plus (rotateRight wg)) wp


rotateLeft : Game -> Game
rotateLeft g =
    Game [g] []


rotateRight : Game -> Game
rotateRight g =
    Game [] [g]

-- xx: List ((Game -> Game) -> Game) -> (Game -> Game) -> Game
-- xx = List.foldl (|>) (\k_ -> k_ << merge zero)


toReplaceableList : TTT -> List ( BWE, BWE -> TTT )
toReplaceableList ( ( a, b, c ), ( d, e, f ), ( g, h, i ) ) =
    [ ( a, \x -> fromList [ x, b, c, d, e, f, g, h, i ] )
    , ( b, \x -> fromList [ a, x, c, d, e, f, g, h, i ] )
    , ( c, \x -> fromList [ a, b, x, d, e, f, g, h, i ] )
    , ( d, \x -> fromList [ a, b, c, x, e, f, g, h, i ] )
    , ( e, \x -> fromList [ a, b, c, d, x, f, g, h, i ] )
    , ( f, \x -> fromList [ a, b, c, d, e, x, g, h, i ] )
    , ( g, \x -> fromList [ a, b, c, d, e, f, x, h, i ] )
    , ( h, \x -> fromList [ a, b, c, d, e, f, g, x, i ] )
    , ( i, \x -> fromList [ a, b, c, d, e, f, g, h, x ] )
    ]


fromList : List BWE -> TTT
fromList l =
    case l of
        a :: b :: c :: d :: e :: f :: g :: h :: i :: [] ->
            ( ( a, b, c ), ( d, e, f ), ( g, h, i ) )

        _ ->
            ( ( E, E, E ), ( E, E, E ), ( E, E, E ) )


emptyTTT : TTT
emptyTTT =
    fromList []


moves : BWE -> TTT -> List TTT
moves c g =
    if bwon g || wwon g then
        []

    else
        let

            folder ( e, r ) a =
                if e == E then
                    r c :: a

                else
                    a
        in
        g |> toReplaceableList |> List.foldl folder []


bwon : TTT -> Bool
bwon g =
    case g of
        ( ( B, B, B ), _, _ ) ->
            True

        ( _, ( B, B, B ), _ ) ->
            True

        ( _, _, ( B, B, B ) ) ->
            True

        ( ( B, _, _ ), ( B, _, _ ), ( B, _, _ ) ) ->
            True

        ( ( _, B, _ ), ( _, B, _ ), ( _, B, _ ) ) ->
            True

        ( ( _, _, B ), ( _, _, B ), ( _, _, B ) ) ->
            True

        ( ( B, _, _ ), ( _, B, _ ), ( _, _, B ) ) ->
            True

        ( ( _, _, B ), ( _, B, _ ), ( B, _, _ ) ) ->
            True

        _ ->
            False


wwon : TTT -> Bool
wwon g =
    case g of
        ( ( W, W, W ), _, _ ) ->
            True

        ( _, ( W, W, W ), _ ) ->
            True

        ( _, _, ( W, W, W ) ) ->
            True

        ( ( W, _, _ ), ( W, _, _ ), ( W, _, _ ) ) ->
            True

        ( ( _, W, _ ), ( _, W, _ ), ( _, W, _ ) ) ->
            True

        ( ( _, _, W ), ( _, _, W ), ( _, _, W ) ) ->
            True

        ( ( W, _, _ ), ( _, W, _ ), ( _, _, W ) ) ->
            True

        ( ( _, _, W ), ( _, W, _ ), ( W, _, _ ) ) ->
            True

        _ ->
            False


type Game
    = Game (List Game) (List Game)


toString : Game -> String
toString game =
    if left game == [] && right game == [] then
        "0"

    else if left game == [zero] && right game == [zero] then
        "*"

    else
        "{" ++ (left game |> List.map toString |> String.join ",") ++ "|" ++ (right game |> List.map toString |> String.join ",") ++ "}"


type Outcome
    = P
    | N
    | L
    | R


merge : Game -> Game -> Game
merge (Game gl gr) (Game hl hr) =
    Game (gl ++ hl) (gr ++ hr)


zero : Game
zero =
    Game [] []


one : Game
one =
    Game [ zero ] []


two : Game
two =
    Game [ one ] []


star : Game
star =
    Game [ zero ] [ zero ]


up : Game
up =
    Game [ zero ] [ star ]


left : Game -> List Game
left (Game l _) =
    l


right : Game -> List Game
right (Game _ r) =
    r


canLForce : Game -> Bool
canLForce l =
    l |> left |> List.any (not << canRForce)


canRForce : Game -> Bool
canRForce =
    right >> List.any (not << canLForce)


outcome : Game -> Outcome
outcome game =
    case ( canLForce game, canRForce game ) of
        ( False, False ) ->
            P

        ( True, False ) ->
            L

        ( False, True ) ->
            R

        ( True, True ) ->
            N


plus : Game -> Game -> Game
plus g h =
    Game
        ((left g |> List.map (plus h))
            ++ (left h |> List.map (plus g))
        )
        ((right g |> List.map (plus h))
            ++ (right h |> List.map (plus g))
        )


negate : Game -> Game
negate g =
    Game (right g |> List.map negate) (left g |> List.map negate)


minus : Game -> Game -> Game
minus g h =
    plus g (negate h)


gte : Game -> Game -> Bool
gte g h =
    [ P, L ] |> List.any ((==) (minus g h |> outcome))


flip : (c -> b -> a) -> b -> c -> a
flip f a b =
    f b a


lte : Game -> Game -> Bool
lte =
    flip gte


gt : Game -> Game -> Bool
gt g h =
    not <| lte g h


lt : Game -> Game -> Bool
lt g h =
    not <| gte g h


eq : Game -> Game -> Bool
eq g h =
    (minus g h |> outcome) == P


undom : Game -> Game
undom (Game l r) =
    let
        roll rel e a =
            if a |> List.any (rel e) then
                a

            else
                e :: (a |> List.filter (not << flip rel e))
    in
    Game (l |> List.foldl (roll lte) []) (r |> List.foldl (roll gte) [])


unrev : Game -> Game
unrev ((Game l r) as g) =
    let
        roll us them rel e a =
            let
                reversals =
                    them e |> List.filter (rel g)
            in
            if List.isEmpty reversals then
                e :: a

            else
                List.concatMap us reversals ++ a
    in
    undom <| Game (l |> List.foldl (roll left right gte) [] |> List.map unrev) (r |> List.foldl (roll right left lte) [] |> List.map unrev)
